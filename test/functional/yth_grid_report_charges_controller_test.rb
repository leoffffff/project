require 'test_helper'

class YthGridReportChargesControllerTest < ActionController::TestCase
  setup do
    @yth_grid_report_charge = yth_grid_report_charges(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:yth_grid_report_charges)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create yth_grid_report_charge" do
    assert_difference('YthGridReportCharge.count') do
      post :create, yth_grid_report_charge: {  }
    end

    assert_redirected_to yth_grid_report_charge_path(assigns(:yth_grid_report_charge))
  end

  test "should show yth_grid_report_charge" do
    get :show, id: @yth_grid_report_charge
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @yth_grid_report_charge
    assert_response :success
  end

  test "should update yth_grid_report_charge" do
    put :update, id: @yth_grid_report_charge, yth_grid_report_charge: {  }
    assert_redirected_to yth_grid_report_charge_path(assigns(:yth_grid_report_charge))
  end

  test "should destroy yth_grid_report_charge" do
    assert_difference('YthGridReportCharge.count', -1) do
      delete :destroy, id: @yth_grid_report_charge
    end

    assert_redirected_to yth_grid_report_charges_path
  end
end
