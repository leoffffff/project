class CreateDailyFixedLineBroadbandDevelopments < ActiveRecord::Migration
  def change
    create_table :daily_fixed_line_broadband_developments do |t|

      t.timestamps
    end
  end
end
