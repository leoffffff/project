class CreateDevelopmentPrepaidUserDetails < ActiveRecord::Migration
  def change
    create_table :development_prepaid_user_details do |t|

      t.timestamps
    end
  end
end
