# encoding: utf-8
#
# 广东联通MSS系统统一认证服务平台接口（规范V1.2.6）
# 测试运行环境：ruby 1.9.3-p125
# 注意：需要安装以下两个gems包
# soap4r-ruby1.9 (2.0.5)
# happymapper (0.4.0)
# gem 'soap4r-ruby1.9'
# gem 'happymapper'
# Sail Lee 2013.6
# xiaofanli@chinaunicom.cn
#
require 'rubygems'
require 'soap/wsdlDriver'
require 'openssl'
require 'base64'
require 'xml'
require 'rexml/document'
require 'happymapper'

# 广东联通目录间接访问接口公钥（XML格式）
PUB_KEY_XML_STRING = '<RSAKeyValue><Modulus>ma+lXgSUOx73xNJWgGdg309gUAmC18G6GKYlERmAnZ1BQQPyaZ4mlzbi3A+'+
    '4FKTWsSSm/FxhJ2vq2e+TpdZ2yHNGlHehxQ65vjyPqWFG1d6+Gvb1RW6NB3yB4d5CsJRkV3CEbotxT/3U'+
    'CcV3txebo+w2BIqtv/qTRXgaxygR1i0=</Modulus><Exponent>AQAB</Exponent></RSAKeyValue>'

# 广东联通目录间接访问接口中地市接口应用程序名称和版本号
# 可用
APP_NAME = 'hemingwork'
APP_VERSION = '6.3'

# 肇庆分公司代码
COMPANY_ID = '23'

# 可用
#APP_NAME = 'moa'
#APP_VERSION = '1.0.1'

#APP_NAME = 'test'
#APP_VERSION = 'v1'

# 广东联通目录间接访问接口WSDL链接
URL = 'http://oa.unicomgd.com/LdapWebInterface/LdapBase1_5.asmx?WSDL'
#URL = 'http://10.118.5.11/LdapWebInterface/LdapBase1_5.asmx?WSDL'
#10.118.5.10
#10.118.5.11
#URL = 'http://10.210.8.8/LdapWebInterface/LdapBase1_6.asmx?WSDL'

# 广东联通目录间接访问接口GetAllDuty调用返回的职务信息类
class OA_Duty
  include HappyMapper
  tag 'Table'

  element :duty_id, String, :tag => 'duty_ID'
  element :company_id, String, :tag => 'Company_ID'
  element :duty_name, String, :tag => 'duty_Name'
  element :duty_level, String, :tag => 'Duty_Level'
end

# 广东联通目录间接访问接口GetAllDepartment调用返回的部门信息类
class OA_Department
  include HappyMapper
  tag 'Table'

  element :dept_id, String, :tag => 'dept_id'
  element :company_id, String, :tag => 'company_id'
  element :dept_name, String, :tag => 'dept_name'
  element :email, String, :tag => 'email'
  element :dept_type, String, :tag => 'dept_type'
  element :dept_gb, String, :tag => 'dept_gb'
  element :dept_en, String, :tag => 'dept_en'
  element :dept_water, String, :tag => 'dept_water'
  element :orderby, String, :tag => 'orderby'
  element :parent_id, String, :tag => 'parent_id'
  element :ldap_dept_id, String, :tag => 'ldap_dept_id'
end

# 广东联通目录间接访问接口GetAllPerson调用返回的个人信息类
class OA_Person
  include HappyMapper
  tag 'Table'

  element :oa_id, String, :tag => 'OA_ID'
  element :oa_name, String, :tag => 'OA_NAME'
  element :oa_com_id, String, :tag => 'OA_COM_ID'
  element :oa_phone, String, :tag => 'OA_PHONE'
  element :oa_mail, String, :tag => 'OA_MAIL'
  element :oa_com_name, String, :tag => 'OA_COM_NAME'
  element :oa_dep_id, String, :tag => 'OA_DEP_ID'
  element :oa_dep_name, String, :tag => 'OA_DEP_NAME'
  element :oa_job_id, String, :tag => 'OA_JOB_ID'
  element :oa_job_name, String, :tag => 'OA_JOB_NAME'
  element :leaderflag, String, :tag => 'Leaderflag'
  element :user_code, String, :tag => 'user_code'
  element :company_code, String, :tag => 'company_code'
  element :dept_code, String, :tag => 'dept_code'
  element :user_type, String, :tag => 'USER_TYPE'
  element :us_order, String, :tag => 'US_Order'
  element :co_order, String, :tag => 'CO_Order'
  element :de_order, String, :tag => 'DE_Order'
  element :du_order, String, :tag => 'DU_Order'
  element :cu_mail, String, :tag => 'CU_MAIL'
end

# 广东联通目录间接访问接口GetPersonByCompanyid调用返回的个人信息类
class OA_People
  include HappyMapper
  tag 'Table'

  element :user_id, String, :tag => 'USER_ID'
  element :dept_id, String, :tag => 'Dept_ID'
  element :duty_id, String, :tag => 'duty_ID'
  element :user_name, String, :tag => 'USER_NAME'
  element :user_sex, String, :tag => 'USER_SEX'
  element :mobile_phone, String, :tag => 'MOBILE_PHONE'
  element :office_phone1, String, :tag => 'OFFICE_PHONE1'
  element :office_phone2, String, :tag => 'OFFICE_PHONE2'
  element :fax_phone, String, :tag => 'FAX_PHONE'
  element :oicq_num, String, :tag => 'OICQ_NUM'
  element :company_id, String, :tag => 'Company_ID'
  element :user_code, String, :tag => 'user_code'
  element :cumail, String, :tag => 'cumail'
end

# 广东联通目录间接访问接口GetAllCompany调用返回的公司信息类
class OA_Company
  include HappyMapper
  tag 'Table'

  element :company_id, String, :tag => 'company_id'
  element :parent_id, String, :tag => 'parent_id'
  element :company_name, String, :tag => 'company_name'
  element :company_gb, String, :tag => 'company_gb'
  element :ldap_company_id, String, :tag => 'ldap_company_id'
  element :address, String, :tag => 'address'
  element :fax, String, :tag => 'fax'
  element :telephone, String, :tag => 'telephone'
end

# 广东联通目录间接访问接口GetAllDepartment调用返回的部门信息数据集
class Duty_Tables
  include HappyMapper
  tag 'gd'

  has_many :duties, OA_Duty
end

# 广东联通目录间接访问接口GetAllDuty调用返回的职务信息数据集
class Department_Tables
  include HappyMapper
  tag 'gd'

  has_many :departments, OA_Department
end

# 广东联通目录间接访问接口GetPersonByCompanyid调用返回的个人信息数据集
class People_Tables
  include HappyMapper
  tag 'gd'

  has_many :people, OA_People
end

# 广东联通目录间接访问接口GetPersonByCompanyid调用返回的个人信息数据集
class Person_Tables
  include HappyMapper
  tag 'gd'

  has_many :persons, OA_Person
end

# 广东联通目录间接访问接口GetAllCompany调用返回的公司信息数据集
class Company_Tables
  include HappyMapper
  tag 'gd'

  has_many :companies, OA_Company
end

# 广东联通目录间接访问接口Auth_CommMethod调用（认证函数）返回的结果类
class Auth_Return
  include HappyMapper
  tag 'return'

  element :flag, String, :tag => 'flag'
  element :err_code, String, :tag => 'errCode'
  element :err_info, String, :tag => 'errinfo'
end

# 广东联通目录间接访问接口PowerThrough调用（应用认证令牌）返回的结果类
class Power_Through
  include HappyMapper
  tag 'PowerThrough'
  element :power_through_info, String, :tag => 'PowerThroughInfo'
  element :token, String, :tag => 'PowerThroughKey'
end

# 广东联通目录间接访问接口PowerThrough调用（应用认证令牌）返回的结果数据集
class Token_Tables
  include HappyMapper
  tag 'gd'

  has_one :power_through, Power_Through
end


# 公钥加密函数
# @param [Object] public_key
# @param [Object] message_string
# @return string
def rsa_encrypt(public_key, message_string)
  # 返回加密后的Base64字符串
  Base64.strict_encode64(public_key.public_encrypt(message_string.encode('UTF-16LE')))
  #Base64.encode64(public_key.public_encrypt(message_string.encode('UTF-16LE'))).rstrip
end

# 公钥生成函数
# @param [Object] xml_string
# @return [Object]
def rsa_public_key(xml_string)
  d = XML::Parser.string(xml_string).parse
  m = Base64.decode64(d.find_first('Modulus').content)
  e = Base64.decode64(d.find_first('Exponent').content)

  pub_key = OpenSSL::PKey::RSA.new
  # modulues
  pub_key.n = OpenSSL::BN.new m, 2
  # exponent
  pub_key.e = OpenSSL::BN.new e, 2
  # return Public Key
  pub_key
end

def oa_soap_drv
  drv = SOAP::WSDLDriverFactory.new(URL).create_rpc_driver
  # 如需记录Web Service通信日志，将以下两行代码注释去掉
  # drv.wiredump_dev = STDERR if $DEBUG
  # drv.wiredump_file_base = 'log_file'

  drv
end

# OA系统认证函数
# @param [Object] username
# @param [Object] password
# @return [Object]
def oa_authenticate(username, password)
  pk = rsa_public_key(PUB_KEY_XML_STRING)

  drv = oa_soap_drv

  param_string = '<auth_info>'
  param_string += '<appname>'+APP_NAME+'</appname><appversion>'+APP_VERSION+'</appversion>'
  param_string += '<cumail>'+rsa_encrypt(pk, username)+'</cumail><password>'+rsa_encrypt(pk, password)+'</password>'
  param_string += '</auth_info>'

  result = Auth_Return.parse(drv.Auth_CommMethod(strConditionXML: param_string).auth_CommMethodResult)

  # 返回认证结果
  result.flag.upcase == 'TRUE'
end

def oa_get_token
  pk = rsa_public_key(PUB_KEY_XML_STRING)

  drv = oa_soap_drv

  result = Token_Tables.parse(drv.PowerThrough(appname: rsa_encrypt(pk, APP_NAME), AppVersion: rsa_encrypt(pk, APP_VERSION)).powerThroughResult)

  result.power_through.token if result.power_through.power_through_info.to_s.upcase == 'TRUE'
end

def oa_get_all_duties(token)
  pk = rsa_public_key(PUB_KEY_XML_STRING)

  drv = oa_soap_drv

  result = Duty_Tables.parse(drv.GetAllDuty(key: token).getAllDutyResult)

  result.duties
end

def oa_get_all_departments(token)
  pk = rsa_public_key(PUB_KEY_XML_STRING)

  drv = oa_soap_drv

  result = Department_Tables.parse(drv.GetAllDepartment(key: token).getAllDepartmentResult)

  result.departments
end

def oa_get_all_people(token)
  pk = rsa_public_key(PUB_KEY_XML_STRING)

  drv = oa_soap_drv

  result = People_Tables.parse(drv.GetPersonByCompanyid(Companyid: COMPANY_ID, key: token).getPersonByCompanyidResult)

  result.people
end

def oa_get_all_companies(token)
  pk = rsa_public_key(PUB_KEY_XML_STRING)

  drv = oa_soap_drv

  result = Company_Tables.parse(drv.GetAllCompany(key: token).getAllCompanyResult)

  result.companies
end

def oa_get_all_persons(token)
  pk = rsa_public_key(PUB_KEY_XML_STRING)

  drv = oa_soap_drv

  result = Person_Tables.parse(drv.GetAllPerson(key: token).getAllPersonResult)

  result.persons
end


# 测试代码
# puts '认证通过！' if oa_authenticate('xiaofanli','Abcd1234L')
# puts '应用访问密钥为：'+oa_get_token
