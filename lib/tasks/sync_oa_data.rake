# encoding: UTF-8
require 'oa_helper'

namespace :db do
  desc '测试'
  task :sync_oa_data => :environment do
    puts '正在同步OA数据...'
    # puts OaPerson.first.user_name
    SYNC_USERNAME = 'chenjf100'
    SYNC_PASSWORD = 'yw#44GB#'

    begin
      if oa_authenticate(SYNC_USERNAME,SYNC_PASSWORD)
        puts '已通过登陆OA系统认证.'
        token_string ||= oa_get_token
        puts '获取OA系统令牌: ' + token_string
        puts '正在获取OA系统用户数据...'
        people ||= oa_get_all_people(token_string)
        puts '用户数据总量为: ' + people.length.to_s if people
        puts '正在清空原有数据...'
        begin
          OaPerson.connection.execute("truncate table #{OaPerson.table_name}")
          puts '已成功清空原有用户数据.'
          puts '正在插入用户数据...'
          OaPerson.transaction do
            people.each do |p|
              begin
                OaPerson.connection.execute "insert into #{OaPerson.table_name} values ('#{p.user_id}','#{p.user_name}','#{p.dept_id}','#{p.duty_id}',#{p.user_sex},'#{p.mobile_phone}','#{p.office_phone1}','#{p.office_phone2}','#{p.fax_phone}','','#{p.company_id}','#{p.user_code}','#{p.cumail}')"
              rescue
                puts "insert into #{OaPerson.table_name} values ('#{p.user_id}','#{p.user_name}','#{p.dept_id}','#{p.duty_id}',#{p.user_sex},'#{p.mobile_phone}','#{p.office_phone1}','#{p.office_phone2}','#{p.fax_phone}','','#{p.company_id}','#{p.user_code}','#{p.cumail}')"
              end
            end
          end
          puts 'OA数据同步成功.'
        rescue
          puts '清空原有用户数据失败!!!'
        end
      else
        puts 'OA系统认证失败, 请检查相关参数!!!'
      end
    rescue
      puts '同步OA数据异常, 请重新尝试!!!'
    end

  end
end
