class DevelopmentPrepaidUserDetailsController < ApplicationController
  # GET /development_prepaid_user_details
  # GET /development_prepaid_user_details.json
  def index
    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @development_prepaid_user_details.to_xml}
      format.json do
        @count, @development_prepaid_user_details = DevelopmentPrepaidUserDetail.get_pages(params[:key],params[:pageIndex], params[:pageSize],params[:sortField], params[:sortOrder])
        @result = {total: @count, data: @development_prepaid_user_details}
        render json: @result
      end
    end
  end
end
