class AfterShiftingNetworkUserDevelopmentFeeDetailsController < ApplicationController
  # GET /after_shifting_network_user_development_fee_details
  # GET /after_shifting_network_user_development_fee_details.json
  def index
    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @after_shifting_network_user_development_fee_details.to_xml}
      format.json do
        @count, @after_shifting_network_user_development_fee_details = AfterShiftingNetworkUserDevelopmentFeeDetail.get_pages(params[:key],params[:pageIndex], params[:pageSize],params[:sortField], params[:sortOrder])
        @result = {total: @count, data: @after_shifting_network_user_development_fee_details}
        render json: @result
      end
    end
  end
end
