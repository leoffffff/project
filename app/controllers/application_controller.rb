# encoding: UTF-8
class ApplicationController < ActionController::Base
  protect_from_forgery

  protected

  private

  def login_required
    logged_in? || access_denied
  end

  def right_required
    current_rights.include?(controller_name) || access_denied
  end

  def access_denied
    reset_session
    redirect_to login_path
    false
  end

  #
  # current系列方法是读取存放在Session或Cookie中的与当前用户相关状态或变量
  #
  def current_user
    if session[:agent_code] != ''
      @current_user ||= Person.find(session[:staff_id]) if session[:staff_id]
    else
      @current_user ||= OaPerson.find(session[:staff_id]) if session[:staff_id]
    end
  end
  helper_method :current_user

  alias :logged_in? :current_user

  def current_roles
    @current_roles ||= Role.where(:id => session[:role_ids]).pluck(:id) if session[:role_ids]
  end
  helper_method :current_roles

  def current_rights
    @current_rights ||= Role.where(:id => session[:role_ids]).inject([]) { |m, r| m + r.menus.pluck(:controller_id)} if session[:role_ids]
  end
  helper_method :current_rights

  def current_agent_code
    @current_agent_code ||= session[:agent_code] if session[:agent_code]
  end
  helper_method :current_agent_code

  def current_channel_ids
    @current_channel_ids ||= Channel.get_chnl_cds_by_role(session[:role_ids],session[:agent_code]) if session[:role_ids] && session[:agent_code].blank?
  end
  helper_method :current_channel_ids

end

