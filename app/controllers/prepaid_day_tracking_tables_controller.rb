class PrepaidDayTrackingTablesController < ApplicationController
  # GET /prepaid_day_tracking_tables
  # GET /prepaid_day_tracking_tables.json
  def index
    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @prepaid_day_tracking_tables.to_xml}
      format.json do
        @count, @prepaid_day_tracking_tables = PrepaidDayTrackingTable.get_pages(params[:key],params[:pageIndex], params[:pageSize],params[:sortField], params[:sortOrder])
        @result = {total: @count, data: @prepaid_day_tracking_tables}
        render json: @result
      end
    end
  end
end
