class PayDayAfterTrackingTablesController < ApplicationController
  # GET /pay_day_after_tracking_tables
  # GET /pay_day_after_tracking_tables.json
  def index
    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @pay_day_after_tracking_tables.to_xml}
      format.json do
        @count, @pay_day_after_tracking_tables = PayDayAfterTrackingTable.get_pages(params[:key],params[:pageIndex], params[:pageSize],params[:sortField], params[:sortOrder])
        @result = {total: @count, data: @pay_day_after_tracking_tables}
        render json: @result
      end
    end
  end
end
