class GroupRevenueDepartmentRealTimeDailysController < ApplicationController
  # GET /group_revenue_department_real_time_dailys
  # GET /group_revenue_department_real_time_dailys.json
  def index
    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @group_revenue_department_real_time_dailys.to_xml}
      format.json do
        @count, @group_revenue_department_real_time_dailys = GroupRevenueDepartmentRealTimeDaily.get_pages(params[:key],params[:pageIndex], params[:pageSize],params[:sortField], params[:sortOrder])
        @result = {total: @count, data: @group_revenue_department_real_time_dailys}
        render json: @result
      end
    end
  end
end
