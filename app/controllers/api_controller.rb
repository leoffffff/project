# encoding: UTF-8
class ApiController < ApplicationController
  before_filter :login_required
  skip_before_filter :right_required, :only => [:get_depts_by_roles]

  def get_oa_people
    OaPerson.select('user_id, user_name')
  end

  def get_agent_people
    Channel.select('chnl_id as user_id, chnl_name as user_name')
  end

  def get_people
    @people = get_oa_people + get_agent_people
    @people.select! {|p| p.user_name.include?(params[:key])} unless params[:key].blank?
    @people.sort_by!(&:user_name) unless params[:sortField].blank?
    render json: {total: @people.size, data: @people[params[:pageIndex].to_i*params[:pageSize].to_i..params[:pageIndex].to_i*params[:pageSize].to_i+params[:pageSize].to_i-1]}
  end

  def get_areas
    if params[:area_id].blank?
      render json: Channel.get_areas
    else
      render json: Channel.get_areas(params[:area_id])
    end
  end

  def get_centers
    if params[:area_id].blank?
      render json: Channel.get_centers
    else
      render json: Channel.get_centers(params[:area_id])
    end
  end

  def get_grids
    if params[:center_id].blank?
      render json: Channel.get_grids
    else
      render json: Channel.get_grids(params[:center_id])
    end
  end

  def get_depts
    if params[:grid_id].blank?
      render json: Channel.get_depts
    else
      render json: Channel.get_depts(params[:grid_id])
    end
  end

  def get_depts_by_roles
    @count, @channels = Channel.get_role_depts(current_roles,current_agent_code,params[:key],params[:pageIndex], params[:pageSize], params[:sortField], params[:sortOrder])
    render json: {total: @count, data: @channels}
  end
end
