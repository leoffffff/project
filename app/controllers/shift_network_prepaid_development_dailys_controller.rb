class ShiftNetworkPrepaidDevelopmentDailysController < ApplicationController
  # GET /shift_network_prepaid_development_dailys
  # GET /shift_network_prepaid_development_dailys.json
  def index
    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @shift_network_prepaid_development_dailys.to_xml}
      format.json do
        @count, @shift_network_prepaid_development_dailys = ShiftNetworkPrepaidDevelopmentDaily.get_pages(params[:key],params[:pageIndex], params[:pageSize],params[:sortField], params[:sortOrder])
        @result = {total: @count, data: @shift_network_prepaid_development_dailys}
        render json: @result
      end
    end
  end
end
