class MarketingRealTimeDailyIncomesController < ApplicationController
  # GET /marketing_real_time_daily_incomes
  # GET /marketing_real_time_daily_incomes.json
  def index
    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @marketing_real_time_daily_incomes.to_xml}
      format.json do
        @count, @marketing_real_time_daily_incomes = MarketingRealTimeDailyIncome.get_pages(params[:key],params[:pageIndex], params[:pageSize],params[:sortField], params[:sortOrder])
        @result = {total: @count, data: @marketing_real_time_daily_incomes}
        render json: @result
      end
    end
  end
end
