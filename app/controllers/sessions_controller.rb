# encoding: UTF-8
class SessionsController < ApplicationController
  layout 'login'

  # Session模块是为了把与当前用户相关的状态或变量写入Session或Cookie中
  def create
    if agent_login_successful?
      if has_role?
        redirect_to welcome_path
        return
      else
        flash_alert_no_right
      end
    else
      if employee_login_successful?
        if has_role?
          redirect_to welcome_path
          return
        else
          flash_alert_no_right
        end
      else
        flash_alert_no_user
      end
    end
    redirect_to root_path
  end

  def new
  end

  def destroy
    reset_session
    redirect_to root_path
  end

  def agent_login_successful?
    @person = Person.authenticate(params[:username],params[:password])
    if @person
      session[:staff_id] = @person.staff_id
      session[:agent_code] = @person.staff_id
      return true
    end
    false
  end

  def has_role?
    @role_ids = current_user.roles.this_system.pluck(:id)
    if @role_ids
      session[:role_ids] = @role_ids
      return true
    end
    false
  end

  def employee_login_successful?
    @oaperson = OaPerson.authenticate(params[:username],params[:password]) if Rails.env.production?
    @oaperson = OaPerson.find_by_cumail("#{params[:username].downcase}@chinaunicom.cn") if Rails.env.development?

    if @oaperson
      session[:staff_id] = @oaperson.user_id
      session[:agent_code] = ''
      return true
    end
    false
  end

  def flash_alert_no_right
    flash[:alert]='权限不足，无权使用该系统，请联系相关业务部门！'
  end

  def flash_alert_no_user
    flash[:alert]='用户名或密码错误，请重新登录！'
  end


end
