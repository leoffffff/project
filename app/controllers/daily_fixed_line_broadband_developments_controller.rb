class DailyFixedLineBroadbandDevelopmentsController < ApplicationController
  # GET /daily_fixed_line_broadband_developments
  # GET /daily_fixed_line_broadband_developments.json
  def index
    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @daily_fixed_line_broadband_developments.to_xml}
      format.json do
        @count, @daily_fixed_line_broadband_developments = DailyFixedLineBroadbandDevelopment.get_pages(params[:key],params[:pageIndex], params[:pageSize],params[:sortField], params[:sortOrder])
        @result = {total: @count, data: @daily_fixed_line_broadband_developments}
        render json: @result
      end
    end
  end
end
