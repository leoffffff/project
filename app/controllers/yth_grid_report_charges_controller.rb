# encoding: UTF-8
class YthGridReportChargesController < ApplicationController
  # GET /yth_grid_report_charges
  # GET /yth_grid_report_charges.json
  def index
    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @zq_loc_grid_tree_tmps.to_xml}
      format.json do
        @count, @yth_grid_report_charges = YthGridReportCharge.get_my_value(params[:key], \
                                                                 params[:pageIndex], params[:pageSize], \
                                                                 params[:sortField], params[:sortOrder],false)
        @result = {total: @count, data: @yth_grid_report_charges}
        render json: @result
      end
    end
  end
end
