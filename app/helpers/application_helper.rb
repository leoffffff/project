# encoding: UTF-8
module ApplicationHelper
  def last_two_month
    (DateTime.now<<2).strftime("%Y-%m")
  end

  def current_agent_code_value
    current_agent_code.blank? ? nil : current_agent_code
  end

  def current_agent_code_is_blank
    current_agent_code.blank? ? 'true' : 'false'
  end

  def welcome_get_menus_by_role_path
    "#{welcome_path}/get_menus_by_role"
  end

  def current_user_name
    return current_user.user_name if current_user && current_agent_code.blank?
    current_user.staff_name
  end

  def current_roles_name
    Role.get_roles_name(current_roles)
  end
end
