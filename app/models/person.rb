class Person < ActiveRecord::Base
  establish_connection :system
  self.abstract_class = true
  
  self.table_name = :ygyj_staff
  self.primary_key = :staff_id

  has_many :person2role_relations, foreign_key: :staff_id
  has_many :roles, foreign_key: :staff_id, through: :person2role_relations

  def self.authenticate(user,passwd)
    @person = Person.find_by_staff_id(user.downcase)
    if @person
      @person.pwd == passwd ? @person : nil
    else
      nil
    end
  end

  def self.get_authorize_scopes(person_id)
    @person = Person.find(person_id)
    [{:area_code => @person.area_id, :yf_id => @person.center_id, :grid_id => @person.grid_id, :chnl_id => @person.staff_id}]
  end

end