require 'oa_helper'
class OaPerson < ActiveRecord::Base
  establish_connection :system
  self.abstract_class = true

  self.table_name = 'dc.oa_people'
  self.primary_key = :user_id

  has_many :person2role_relations, foreign_key: :staff_id
  has_many :roles, foreign_key: :staff_id, through: :person2role_relations
  # has_many :sources, :class_name => 'DeptTree', through: :oa_persons_sources_relationships, foreign_key: :user_id


  def self.authenticate(user,passwd)
    oa_authenticate(user, passwd) ? @person = OaPerson.find_by_cumail(user+'@chinaunicom.cn') : nil
  end

  def self.get_pages(key,index,size,sort_field,sort_order)
    sql =<<SQL
      select p.user_id,
             p.user_name,
             d.dept_name,
             p.mobile_phone
        from dc.oa_people p, dc.oa_departments d
       where p.dept_id = d.dept_id
SQL
    sql += " and p.user_name like '%#{key}%' " unless key.blank?
    sql += " order by #{sort_field} #{sort_order}" unless sort_field.blank?
    @people = OaPerson.find_by_sql(sql)
    return @people.count, @people[index.to_i*size.to_i..index.to_i*size.to_i+size.to_i-1]
  end
end
