class PrepaidDayTrackingTable < ActiveRecord::Base
  # attr_accessible :title, :body
  self.table_name = 'dc.ssfz_yff_track'
  def self.get_pages(key, index, size, sort_field, sort_order)
    sql =<<SQL
        select a.timest       ,
               a.flag_name    ,
               a.hf_new_day01 "1",
               a.hf_new_day02 "2",
               a.hf_new_day03 "3",
               a.hf_new_day04 "4",
               a.hf_new_day05 "5",
               a.hf_new_day06 "6",
               a.hf_new_day07 "7",
               a.hf_new_day08 "8",
               a.hf_new_day09 "9",
               a.hf_new_day10 "10",
               a.hf_new_day11 "11",
               a.hf_new_day12 "12",
               a.hf_new_day13 "13",
               a.hf_new_day14 "14",
               a.hf_new_day15 "15",
               a.hf_new_day16 "16",
               a.hf_new_day17 "17",
               a.hf_new_day18 "18",
               a.hf_new_day19 "19",
               a.hf_new_day20 "20",
               a.hf_new_day21 "21",
               a.hf_new_day22 "22",
               a.hf_new_day23 "23",
               a.hf_new_day24 "24",
               a.hf_new_day25 "25",
               a.hf_new_day26 "26",
               a.hf_new_day27 "27",
               a.hf_new_day28 "28",
               a.hf_new_day29 "29",
               a.hf_new_day30 "30",
               a.hf_new_day31 "31"
          from dc.ssfz_yff_track a
SQL
    if key.blank?
      sql += " order by #{sort_field} #{sort_order}"
    else
      sql += " where a.flag_name = '#{key}' order by #{sort_field} #{sort_order}"
    end
    @prepaid_day_tracking_tables = PayDayAfterTrackingTable.find_by_sql(sql)
    return @prepaid_day_tracking_tables.count, @prepaid_day_tracking_tables[index.to_i*size.to_i..index.to_i*size.to_i+size.to_i-1]
  end

end
