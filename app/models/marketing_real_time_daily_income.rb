class MarketingRealTimeDailyIncome < ActiveRecord::Base
  # attr_accessible :title, :body
  self.table_name = 'sssr_sc_dayreport_yth'
  def self.get_pages(key, index, size, sort_field, sort_order)

    sql =<<SQL
        select aa.flag_code       ,
               aa.timest          ,
               aa.flag_name       ,
               aa.income_mobile_d ,
               aa.income_ydhff_d  ,
               aa.income_ydyff_d  ,
               aa.income_gw_d     ,
               aa.income_kd_d     ,
               aa.income_mobile_m income_mobile_m1,
               aa.income_ydhff_m  income_ydhff_m1,
               aa.income_ydyff_m  income_ydyff_m1,
               aa.income_gw_m     income_gw_m1,
               aa.income_kd_m     income_kd_m1,
               bb.income_mobile_m income_mobile_m2,
               bb.income_ydhff_m  income_ydhff_m2,
               bb.income_ydyff_m  income_ydyff_m2,
               bb.income_gw_m     income_gw_m2,
               bb.income_kd_m     income_kd_m2
          from (select a.timest,
                       a.flag_code,
                       a.flag_name,
                       a.income_mobile_d,
                       a.income_ydhff_d,
                       a.income_ydyff_d,
                       a.income_gw_d,
                       a.income_kd_d,
                       a.income_mobile_m,
                       a.income_ydhff_m,
                       a.income_ydyff_m,
                       a.income_gw_m,
                       a.income_kd_m
                  from dc.sssr_sc_dayreport_yth a 
                ) aa,
               (select a.timest,
                       a.flag_code,
                       a.flag_name,
                       a.income_mobile_m,
                       a.income_ydhff_m,
                       a.income_ydyff_m,
                       a.income_gw_m,
                       a.income_kd_m
                  from dc.sssr_sc_dayreport_yth a
                 where a.timest = to_char(add_months(sysdate - 1, -1), 'yyyymm') ||
                       to_char(sysdate - 1, 'dd')) bb
         where aa.flag_name = bb.flag_name(+)
           and aa.flag_code = bb.flag_code(+)
SQL
    if key.blank?
      sql += " and aa.timest = to_char(sysdate - 2, 'yyyymmdd') order by #{sort_field} #{sort_order}"
    else
      sql += " and aa.timest = #{key} order by #{sort_field} #{sort_order}"
    end
    @marketing_real_time_daily_incomes = MarketingRealTimeDailyIncome.find_by_sql(sql)
    return @marketing_real_time_daily_incomes.count, @marketing_real_time_daily_incomes[index.to_i*size.to_i..index.to_i*size.to_i+size.to_i-1]
  end


end
