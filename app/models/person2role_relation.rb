class Person2roleRelation < ActiveRecord::Base
  establish_connection :system
  self.abstract_class = true

  attr_accessible :role_id, :staff_id, :creator, :updater, :created_at, :updated_at, :app_id
  self.table_name = 'dc.persons_roles_relationships'
  self.primary_key = :id
  self.sequence_name = 'dc.persons_roles_relations_seq'

  belongs_to :role, foreign_key: :role_id
  belongs_to :person, foreign_key: :staff_id
  # foreign_key is outer table index id, the name is the field name of outer table.
  belongs_to :oa_person, foreign_key: :user_id

  scope :this_system, where("role_id in (#{System.this_system.first.roles.pluck(:id).join(',')})")

  def self.get_pages(key, index, size, sort_field, sort_order)
    sql =<<SQL
      select * from
      (
      select pr.id,
             pr.role_id,
             r.name role_name,
             pr.staff_id,
             p.staff_name,
             pr.creator,
             pr.updater,
             pr.created_at,
             pr.updated_at,
             pr.app_id,
             1 is_agent
        from dc.persons_roles_relationships pr,
             dc.roles                       r,
             yongjin.ygyj_staff             p
       where pr.role_id = r.id(+)
         and pr.staff_id = p.staff_id
         and pr.role_id in (#{System.this_system.first.roles.pluck(:id).join(',')})
      union
      select pr.id,
             pr.role_id,
             r.name role_name,
             pr.staff_id,
             oa.user_name staff_name,
             pr.creator,
             pr.updater,
             pr.created_at,
             pr.updated_at,
             pr.app_id,
             0 is_agent
        from dc.persons_roles_relationships pr,
             dc.roles                       r,
             dc.oa_people                   oa,
             yongjin.ygyj_staff             p
       where pr.role_id = r.id(+)
         and pr.staff_id = oa.user_id
         and pr.role_id in (#{System.this_system.first.roles.pluck(:id).join(',')})
      ) a
SQL

    sql += " where a.role_name like '%#{key}%'" unless key.blank?
    sql += " order by #{sort_field} #{sort_order}" unless sort_field.blank?

    @role2menu_relations = Role2menuRelation.find_by_sql(sql)
    return @role2menu_relations.count, @role2menu_relations[index.to_i*size.to_i..index.to_i*size.to_i+size.to_i-1]
  end

end
