class Comic < ActiveRecord::Base
  attr_accessible :issue, :publisher, :title
end
