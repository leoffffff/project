class AfterShiftingNetworkUserDevelopmentFeeDetail < ActiveRecord::Base
  # attr_accessible :title, :body
  def self.get_pages(key, index, size, sort_field, sort_order)
    sql =<<SQL
        select a.timest         ,
               a.user_id        ,
               a.serial_number  ,
               a.net_type_name  ,
               a.brand_name     ,
               a.product_id     ,
               a.product_name   ,
               a.brand_code_bi  ,
               a.brand_name_bi  ,
               a.newdate        ,
               a.cust_name      ,
               a.depart_id      ,
               a.depart_name    ,
               a.yf_name        ,
               a.area_name      ,
               a.user_type      ,
               a.state_name     ,
               a.ycye           ,
               a.yfye           ,
               a.zkye           ,
               a.sssr           
          from dc.ssfz_hff_user_detail a
SQL
    if key.blank?
      sql += " order by #{sort_field} #{sort_order}"
    else
      sql += " where a.yf_name = '#{key}' order by #{sort_field} #{sort_order}"
    end
    @marketing_real_time_daily_incomes = MarketingRealTimeDailyIncome.find_by_sql(sql)
    return @marketing_real_time_daily_incomes.count, @marketing_real_time_daily_incomes[index.to_i*size.to_i..index.to_i*size.to_i+size.to_i-1]
  end
end
