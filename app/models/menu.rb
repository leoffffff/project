class Menu < ActiveRecord::Base
  establish_connection :system
  self.abstract_class = true

  attr_accessible :id, :text, :pid, :icon, :url, :orderby, :app_id, :status, :controller_id, :action_id
  self.table_name = 'dc.menus'
  self.primary_key = :id
  self.sequence_name = 'dc.menus_seq'

  has_many :role2menu_relations, foreign_key: :menu_id
  has_many :roles, foreign_key: :menu_id, through: :role2menu_relations
  belongs_to :system, foreign_key: :id

  scope :valid, where(:status => 1)
  scope :this_system, where(:app_id => Yongjin::Application.config.system_id)

  def self.get_pages(key,index,size,sort_field,sort_order)
    if key.nil? or key == ''
      @menus = Menu.this_system.order("#{sort_field} #{sort_order}").all
    else
      @menus = Menu.this_system.where('name like ?', "%#{key}%").order("#{sort_field} #{sort_order}")
    end
    return @menus.count, @menus[index.to_i*size.to_i..index.to_i*size.to_i+size.to_i-1]
  end

  def self.get_all
    Menu.select("id as menu_id, text as menu_name").this_system.valid
  end

end