class Rule2modRelation < ActiveRecord::Base
  self.table_name = 'ygyj_rule_mod_relation'
  
  belongs_to :rule_code, foreign_key: :rule_code
  belongs_to :mod_code, foreign_key: :mod_code
end