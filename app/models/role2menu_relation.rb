class Role2menuRelation < ActiveRecord::Base
  establish_connection :system
  self.abstract_class = true

  attr_accessible :role_id, :menu_id

  self.table_name = 'dc.roles_menus_relationships'
  self.primary_key = :id
  self.sequence_name = 'dc.roles_menus_relationships_seq'

  belongs_to :menu, foreign_key: :menu_id
  belongs_to :role, foreign_key: :role_id

  scope :this_system, where("role_id in (#{System.this_system.first.roles.pluck(:id).join(',')})")

  def self.get_pages(key, index, size, sort_field, sort_order)
    sql =<<SQL
      select rm.role_id, r.name role_name, rm.menu_id, m.text menu_name, rm.id
        from dc.roles_menus_relationships rm, dc.roles r, dc.menus m
       where rm.role_id = r.id(+)
         and rm.menu_id = m.id(+)
         and rm.role_id in (#{System.this_system.first.roles.pluck(:id).join(',')})
SQL
    if key.blank?
      sql += " order by #{sort_field} #{sort_order}"
    else
      sql += " and r.name like '%#{key}%' order by #{sort_field} #{sort_order}"
    end
    @role2menu_relations = Role2menuRelation.find_by_sql(sql)
    return @role2menu_relations.count, @role2menu_relations[index.to_i*size.to_i..index.to_i*size.to_i+size.to_i-1]
  end

  def self.get_role2menu_by_id(id)
    sql =<<SQL
      select rm.role_id, r.name role_name, rm.menu_id, m.text menu_name, rm.id
        from dc.roles_menus_relationships rm, dc.roles r, dc.menus m
       where rm.role_id = r.id(+)
         and rm.menu_id = m.id(+)
         and rm.id = ?
SQL
    Role2menuRelation.find_by_sql([sql, id])
  end
end