# encoding: UTF-8
class YthGridReportCharge < ActiveRecord::Base

  attr_accessible :area_name, :yf_name, :grid_name, :jcdy_name, :product_name

  self.table_name='dc.yth_grid_report_charge'

  def self.get_my_value(key,index,size,sort_field,sort_order,is_all)
    if key.nil? or key == ''
      @yth_grid_report_charges = YthGridReportCharge.order("yth_grid_report_charge.#{sort_field} #{sort_order}").all \
                               if YthGridReportCharge.column_names.include?(sort_field)
    else
      @yth_grid_report_charges = YthGridReportCharge.where('yf_name like ?', "%#{key}%") \
                               .order("yth_grid_report_charge.#{sort_field} #{sort_order}") \
                               if YthGridReportCharge.column_names.include?(sort_field)
    end

    if is_all
      return @yth_grid_report_charges
    else
      return @yth_grid_report_charges.count, @yth_grid_report_charges[index.to_i*size.to_i..index.to_i*size.to_i+size.to_i-1]
    end
  end
end
