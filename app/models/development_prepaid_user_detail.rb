class DevelopmentPrepaidUserDetail < ActiveRecord::Base
  # attr_accessible :title, :body
  self.table_name = 'dc.ssfz_yff_user_detail'
  def self.get_pages(key, index, size, sort_field, sort_order)
    sql =<<SQL
        select a.timest        ,
               a.user_id       ,
               a.serial_number ,
               a.cust_name     ,
               a.pspt_id       ,
               a.depart_id     ,
               a.depart_name   ,
               a.product_id    ,
               a.product_name  ,
               a.yf_name       ,
               a.area_name     ,
               a.user_type     ,
               a.newdate       ,
               a.status        ,
               a.ye            ,
               a.ss            
          from dc.ssfz_yff_user_detail a
SQL
    if key.blank?
      sql += " order by #{sort_field} #{sort_order}"
    else
      sql += " where a.yf_name = '#{key}' order by #{sort_field} #{sort_order}"
    end
    @development_prepaid_user_details = DevelopmentPrepaidUserDetail.find_by_sql(sql)
    return @development_prepaid_user_details.count, @development_prepaid_user_details[index.to_i*size.to_i..index.to_i*size.to_i+size.to_i-1]
  end
end
