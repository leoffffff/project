class System < ActiveRecord::Base
  establish_connection :system
  self.abstract_class = true

  self.table_name = 'dc.applications'
  self.primary_key = :id

  has_many :menus, foreign_key: :app_id
  has_many :roles, foreign_key: :app_id

  scope :valid, where(:status => true)
  scope :this_system, where(:id => Yongjin::Application.config.system_id)

  def self.get_all
    System.select("id, name").this_system.valid
  end
end