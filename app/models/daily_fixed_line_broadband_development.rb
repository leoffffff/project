class DailyFixedLineBroadbandDevelopment < ActiveRecord::Base
  # attr_accessible :title, :body
  def self.get_pages(key, index, size, sort_field, sort_order)
    sql =<<SQL
        select a.flag_code  ,
               a.timest		,
               a.flag_name	,
               a.hf_new_day	,
               a.hf_new_mon	,
               to_char(nvl(round((a.hf_new_mon - m.hf_new_mon) /
                                 nvl(m.hf_new_mon, 1),
                                 5) * 100,
                           0),
                       'fm9999999990.00') || '%' ring_growth,
               to_char(nvl(round((a.hf_new_mon - n.hf_new_mon) /
                                 nvl(n.hf_new_mon, 1),
                                 5) * 100,
                           0),
                       'fm9999999990.00') || '%' year_on_year_growth
          from (select a.*
                  from dc.ssfz_kd_dayreport_yth a 
				) a,
               (select m.*
                  from dc.ssfz_kd_dayreport_yth m 
                 where timest = to_char(add_months(sysdate - 1, -1), 'yyyymmdd')) m,
               (select n.*
                  from dc.ssfz_kd_dayreport_yth n 
                 where timest = to_char(add_months(sysdate - 1, -12), 'yyyymmdd')) n
         where a.flag_code = m.flag_code(+)
           and a.flag_code = n.flag_code(+)
SQL
    if key.blank?
      sql += " and a.timest = to_char(sysdate - 2, 'yyyymmdd') order by #{sort_field} #{sort_order}"
    else
      sql += " and a.timest = #{key} order by #{sort_field} #{sort_order}"
    end
    @marketing_real_time_daily_incomes = MarketingRealTimeDailyIncome.find_by_sql(sql)
    return @marketing_real_time_daily_incomes.count, @marketing_real_time_daily_incomes[index.to_i*size.to_i..index.to_i*size.to_i+size.to_i-1]
  end
end
