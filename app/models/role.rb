# encoding: UTF-8
class Role < ActiveRecord::Base
  establish_connection :system
  self.abstract_class = true

  attr_accessible :name, :area_id, :center_id, :grid_id, :dept_id, :app_id, :status

  self.table_name = 'dc.roles'

  self.primary_key = :id

  self.sequence_name ='dc.roles_seq'

  scope :valid, where(:status => 1)
  scope :this_system, where(:app_id => Project::Application.config.system_id)

  belongs_to :system, foreign_key: :id

  has_many :role2menu_relations, foreign_key: :role_id
  has_many :menus, foreign_key: :role_id, through: :role2menu_relations

  has_many :person2role_relations, foreign_key: :role_id
  has_many :persons, foreign_key: :role_id, through: :person2role_relations
  has_many :oa_persons, foreign_key: :role_id, through: :person2role_relations

  def self.get_pages(key,index,size,sort_field,sort_order)
    if key.nil?
      @roles = Role.this_system.order("#{sort_field} #{sort_order}")
    else
      @roles = Role.this_system.where('name like ?', "%#{key}%").order("#{sort_field} #{sort_order}")
    end
    return @roles.count, @roles[index.to_i*size.to_i..index.to_i*size.to_i+size.to_i-1]
  end

  def self.get_all
    Role.select('id as role_id, name as role_name').this_system.valid
  end

  def self.get_authorize_scopes(ids)
    @rs = []
    @roles = Role.find(ids)
    @roles.each {|r| @rs << {:area_code => r.area_id, :yf_id => r.center_id, :grid_id => r.grid_id}} if @roles
    return @rs
  end

  def self.get_roles_name(role_ids)
    Role.where(:id => role_ids).pluck(:name).join("、")
  end

end