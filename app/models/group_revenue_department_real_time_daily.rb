class GroupRevenueDepartmentRealTimeDaily < ActiveRecord::Base
  # attr_accessible :title, :body
  self.table_name = 'sssr_jt_dayreport_yth'

  def self.get_pages(key, index, size, sort_field, sort_order)

    sql =<<SQL
         select aa.flag_code       ,
                aa.timest          ,
                aa.flag_name       ,
                aa.income_mobile_d ,
                aa.income_ydhff_d  ,
                aa.income_wlw_d    ,
                aa.income_ydyff_d  ,
                aa.income_wxwp_d   ,
                aa.income_gw_d     ,
                aa.income_hlwzx_d  ,
                aa.income_zx_d     ,
                aa.income_mobile_m income_mobile_m1,
                aa.income_ydhff_m  income_ydhff_m1,
                aa.income_wlw_m    income_wlw_m1,
                aa.income_ydyff_m  income_ydyff_m1,
                aa.income_wxwp_m   income_wxwp_m1,
                aa.income_gw_m     income_gw_m1,
                aa.income_hlwzx_m  income_hlwzx_m1,
                aa.income_zx_m     income_zx_m1,
                bb.income_mobile_m income_mobile_m2,
                bb.income_ydhff_m  income_ydhff_m2,
                bb.income_wlw_m    income_wlw_m2,
                bb.income_ydyff_m  income_ydyff_m2,
                bb.income_wxwp_m   income_wxwp_m2,
                bb.income_gw_m     income_gw_m2,
                bb.income_hlwzx_m  income_hlwzx_m2,
                bb.income_zx_m     income_zx_m2
           from (select a.timest,
                        a.flag_code,
                        a.flag_name,
                        a.income_mobile_d,
                        a.income_ydhff_d,
                        a.income_wlw_d,
                        a.income_ydyff_d,
                        a.income_wxwp_d,
                        a.income_gw_d,
                        a.income_hlwzx_d,
                        a.income_zx_d,
                        a.income_mobile_m,
                        a.income_ydhff_m,
                        a.income_wlw_m,
                        a.income_ydyff_m,
                        a.income_wxwp_m,
                        a.income_gw_m,
                        a.income_hlwzx_m,
                        a.income_zx_m
                   from dc.sssr_jt_dayreport_yth a
                 ) aa,
                (select a.timest,
                        a.flag_code,
                        a.flag_name,
                        a.income_mobile_m,
                        a.income_ydhff_m,
                        a.income_wlw_m,
                        a.income_ydyff_m,
                        a.income_wxwp_m,
                        a.income_gw_m,
                        a.income_hlwzx_m,
                        a.income_zx_m
                   from dc.sssr_jt_dayreport_yth a
                  where a.timest = to_char(add_months(sysdate - 1, -1), 'yyyymm') ||
                        to_char(sysdate - 1, 'dd')) bb
          where aa.flag_name = bb.flag_name(+)
            and aa.flag_code = bb.flag_code(+)
SQL
    if key.blank?
      sql += " and aa.timest = to_char(sysdate - 2, 'yyyymmdd') order by #{sort_field} #{sort_order}"
    else
      sql += " and aa.timest = #{key} order by #{sort_field} #{sort_order}"
    end
    @group_revenue_department_real_time_dailys = GroupRevenueDepartmentRealTimeDaily.find_by_sql(sql)
    return @group_revenue_department_real_time_dailys.count, @group_revenue_department_real_time_dailys[index.to_i*size.to_i..index.to_i*size.to_i+size.to_i-1]
  end

end
